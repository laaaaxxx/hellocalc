import React, {
  Component
} from 'react';
import './App.css';

var buttonPadding = {"padding-top": "10px" };
var screenPadding = { "border-style":"solid", "width" : "15%", "height": "30px"};

export default class App extends Component {
  state = {
    hello: "Calculator",
    screen: "",
    operation: "",
    prev:""
  }

  numberButton = (event) => {
    // number pressing
    this.setState({
      screen: this.state.screen + event.target.value,
    })
  }

  clearButton = (event) => {
    // Clear button
    this.setState({
      screen: ""
    })
  }

  opButton = (event) => {
    // perform calculation
    if(event.target.value === "calc"){
      if(this.state.operation === "add"){
      this.setState({
        screen: parseInt(this.state.prev) + parseInt(this.state.screen),
        operation: "",
        prev: ""
      })
     } 
     else if(this.state.operation === "minus") {
      this.setState({
        screen: parseInt(this.state.prev) - parseInt(this.state.screen),
        operation: "",
        prev: ""
      })
     }
     else if(this.state.operation === "div") {
      this.setState({
        screen: parseInt(this.state.prev) / parseInt(this.state.screen),
        operation: "",
        prev: ""
      })
     }

     else if(this.state.operation === "mult") {
      this.setState({
        screen: parseInt(this.state.prev) * parseInt(this.state.screen),
        operation: "",
        prev: ""
      })
     }
    } else {
      // Set operation
      this.setState({
        prev: this.state.screen,
        operation: event.target.value,
        screen: ""
      })
    }
  }

  render() {
    return ( 
    <div >
      <h1>{this.state.hello} </h1>
      <h3 style={screenPadding}>{this.state.screen} </h3>
      <div>
        <button value="0" onClick={this.numberButton} > 0 </button>
        <button value="1" onClick={this.numberButton} > 1 </button>
        <button value="2" onClick={this.numberButton} > 2 </button>
        <button value="3" onClick={this.numberButton} > 3 </button>
        <button value="4" onClick={this.numberButton} > 4 </button>
        <button value="5" onClick={this.numberButton} > 5 </button>
        <button value="6" onClick={this.numberButton} > 6 </button>
        <button value="7" onClick={this.numberButton} > 7 </button>
        <button value="8" onClick={this.numberButton} > 8 </button>
        <button value="9" onClick={this.numberButton} > 9 </button>
       </div>
       <div style = {buttonPadding} >
        <button value="add" onClick={this.opButton}> + </button>
        <button value="minus" onClick={this.opButton}> - </button>
        <button value="div" onClick={this.opButton}> / </button>
        <button value="mult" onClick={this.opButton}> x </button>
        <button value="calc" onClick={this.opButton} > = </button>
        <button onClick={this.clearButton}> CLEAR </button>
       </div>
    </div>
    );
  }
}