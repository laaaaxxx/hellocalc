import React from 'react';

const OperationsButtons = (props) =>
    (
       
        <div>
        <button value="add" onClick={props.addButton}> + </button>
         <button value="minus" onClick={props.minusButton}> - </button>
         <button value="div" onClick={props.divButton}> / </button>
         <button value="mult" onClick={props.multButton}> x </button>
         <button value="calc" onClick={props.equalsButton} > = </button>
         <button onClick={props.clearButton}> CLEAR </button>
         <button onClick={props.bkButton}> BACKSPACE </button>
       </div> 
    );

export default OperationsButtons;   
