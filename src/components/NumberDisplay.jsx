import React from 'react';

const NumberDisplay = (props) =>
    (
    <div>
        <h3>{props.displayNumber}</h3>
    </div>
    );

export default NumberDisplay;   
