import React from 'react';

const NumberButtons = (props) =>
    (
    <div>
        <button value="0" onClick={props.pressButton}> 0 </button>
        <button value="1" onClick={props.pressButton}> 1 </button>
        <button value="2" onClick={props.pressButton}> 2 </button>
        <button value="3" onClick={props.pressButton}> 3 </button>
        <button value="4" onClick={props.pressButton}> 4 </button>
        <button value="5" onClick={props.pressButton}> 5 </button>
        <button value="6" onClick={props.pressButton}> 6 </button>
        <button value="7" onClick={props.pressButton}> 7 </button>
        <button value="8" onClick={props.pressButton}> 8 </button>
        <button value="9" onClick={props.pressButton}> 9 </button>
    </div>
    );

export default NumberButtons;   
