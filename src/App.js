import React, {
  Component
} from 'react';
import './App.css';
import NumberButtons from './components/NumberButtons';
import OperationsButtons from './components/OperationsButtons';
import NumberDisplay from './components/NumberDisplay';

var buttonPadding = {"padding-top": "10px" };
var screenPadding = { "border-style":"solid", "width" : "30%", "height": "60px"};



export default class App extends Component {
  state = {
    hello: "Calculator",
    screen: "",
    operation: "",
    prev:""
  }


  // number buttons in screen
  // arrow functions 
  // - cleaner, shorter code
  // - assign ka ng function sa variable
  // - (Variable Name) = (Inside varibale) => { function }
  // - In React, kumukuha ng events para mag trigger ng functions (cause and effect)

  // event(onClick).target(button may zero).value
  numButton = (event) => {
    // number pressing
  if (event.target.value === "0") {
      if (this.state.screen.length < 1) {
        alert('Please select other numbers')
      } else {
        this.setState({screen: this.state.screen + event.target.value})
      }
  } 
  else {
      if (this.state.screen.length <= 12) {
        this.setState({
          screen: this.state.screen + event.target.value
        }) 
      } 
     else {
      alert('12 characters only!')
    }
  }
  }


  clearButton = (event) => {
    // Clear button
    this.setState({
      screen: ""
    })
  }
  
  bkButton = (event) => {
    this.setState({
      screen: this.state.screen.toString().slice(0, -1),
    })
  }

  
  
  equalsButton = event => {
    // may laman yung prev, screen
    if(this.state.operation === "add"){
      this.setState({
        screen : parseInt(this.state.prev) + parseInt(this.state.screen)
      })
    } 
    else if(this.state.operation === "minus"){
      this.setState({
        screen : parseInt(this.state.prev) - parseInt(this.state.screen)
      })
    }
    else if(this.state.operation === "div"){
      this.setState({
        screen : parseInt(this.state.prev) / parseInt(this.state.screen)
      })
    }
    else if(this.state.operation === "mult"){
      this.setState({
        screen : parseInt(this.state.prev) * parseInt(this.state.screen)
      })
    }
    else {
      // Set operation
      this.setState({
        prev: this.state.screen,
        operation: event.target.value,
        screen: ""
      })
    }
  }

  addButton = (event) => {
    this.setState({
      operation: "add",
      prev: this.state.screen,
      screen: ""
    })
  }

  minusButton = (event) => {
    this.setState({
      operation: "minus",
      prev: this.state.screen,
      screen: ""
    })
  }

  divButton = (event) => {
    this.setState({
      operation: "div",
      prev: this.state.screen,
      screen: ""
    })
  }

  multButton = (event) => {
    this.setState({
      operation: "mult",
      prev: this.state.screen,
      screen: ""
    })
  }



  render() {
    return ( 
    <div align="center">
      <h1>{this.state.hello}</h1>
      <div style={screenPadding}>
      <NumberDisplay displayNumber={this.state.screen} />
      </div>
         
        
      {/* Numbers */}
      <NumberButtons pressButton={this.numButton} />


       {/* Operations */}
      <div style={buttonPadding}>
      <OperationsButtons addButton={this.addButton} minusButton={this.minusButton} 
          divButton={this.divButton} multButton={this.multButton} 
          equalsButton={this.equalsButton} clearButton={this.clearButton}
          bkButton={this.bkButton} />
      </div>
    </div>
    );
  }
}